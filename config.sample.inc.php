<?php

/**   Archivo de Ejemplo para la configuraciÃƒÂ³n de conexiÃƒÂ³n a la base de datos
 *   @author Federicorp <federicorp@easypanelphp.com>
 *   @copyright 2015
 *   @version 2.0
 *   @package EasyEnginePHP
 *
 *   Antes de tocar este archivo, asegurece de saber los conceptos de Base de Datos y su conexiÃƒÂ³n.
 *   Edite este archivo y luego guarde como "config.php"
 *
 */

/** En las lineas siguientes podrÃƒÂ¡ modificar las configuraciones de conexiÃƒÂ³n.
 *  Debe modificar siempre la palabra situada luego del simbolo ( => )
 *  Al modificar la palabra antes del simbolo ( => ) puede enfrentar errores en el software.
 *  Ejemplos:
 *    "HOST"  =>    "miservidor.com"           <--- Correcto
 *    "miservidor.com"  =>    "miservidor.com"    <--- Incorrecto
 */

return array(

    //<----------------   ONLY API USE --------------------------------->//
    "ONLY_API" => FALSE,

    //<----------------  CONFIGURACIONES DE SEGURIDAD ------------------>//
    "ENC_SALT" => "put-here-a-random-string",

    //<------------  Configuraciones de Base de datos  ------------------>//

    //------------  Usar Base de Datos  ------------------//
    "USE_BD" =>   TRUE    ,

    "BD" =>  array(

        //------------  Configuracion de primera BD  ------------------//
        array(

            //------  Tipo de conexion  ------//
            "TYPE" =>    "MYSQL"    ,

            //------  Servidor de BD  ------//
            "HOST" =>    "localhost"    , //En algunos casos no hace falta tocar esta lÃƒÂ­nea

            //------  Nombre de BD  ------//
            "NAMEDB" =>    ""    ,

            //------  Usuario de BD  ------//
            "USER" =>    "root"    ,

            //------  Pass de Usuario de BD  ------//
            "PASS" =>    "",

            //------  Puerto de conexion de BD  ------//
            "PORT" => "3306"  //En algunos casos no hace falta tocar esta linea

        ),

        //------------  Configuracion de segunda BD  ------------------//
        array(

        )

    )

);
    