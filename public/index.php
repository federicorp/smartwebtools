<?php
/**
 * Archivo Index del Panel
 *
 * @package EasyEnginePHP
 * @author Federicorp <federicorp@easypanel.com>
 * @copyright 2014
 */

//Composer's Autoload
require_once '../vendor/autoload.php';

//Iniciación del Motor Easy
\EasyEngine\Engine::build()->dispatch();